Insert mod: `i`  
Write and Quit: `:wq`  
Dublicate line: `:t.`  
Auto complate: `ctrl + n`  
Copy Line: `yy` (normal mod)  
Paste: `P` or `p`  
Undo: `u`  
Redo: `ctrl + r`  
Search and Replace: `%s/search/replace/gc` (c for confirm)  
Delete Line: `dd`  
Start of Line: `0`  
End of Line: `$`  
First line of the doc: `gg`  
Last line of the doc: `G`  
Go to line: `5G` (line 5)  
Write with sudo: `:w !sudo tee %`
