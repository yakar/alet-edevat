#!/bin/bash

# 1- DISK BOLUMLEME
# Disk list
i=0
for DISK in `lsblk | grep disk | awk '{ print $1}'`; do
    BOYUT=`lsblk | grep -w $DISK | awk '{ print $4 }'`
    if [ "$BOYUT" != "" ]; then
        MENU[$i]="$DISK $BOYUT"
        ((i++))
    fi
done
if [ "${#MENU[@]}" -gt 1 ]; then
	echo "[!] Disk bulunamadı!"
	exit
fi


# Dialog
dialog --clear --title "DISK SECIN" --menu "Aşağı / Yukarı yön tuşları ile seçebilirsiniz" 12 60 6 ${MENU[*]} 2> /tmp/menusecim.$$

buton=$?
disk=`cat /tmp/menusecim.$$`

if [ $buton == 0 ]; then
	cfdisk /dev/$disk
else
	clear
	echo "[!] Kuruluma devam etmek icin disk secmelisiniz!"
	exit
fi


# 2- FORMAT
echo "[*] Disk EXT4 olarak formatlanıyor.."
mkfs.ext4 -F "/dev/$disk"1


# 3- MOUNT
echo "[*] Disk mount ediliyor.."
mountpoint -q "/dev/$disk"1 || mount "/dev/$disk"1 /mnt


# 4- CP
echo "[*] Kopyalanıyor.."
#cp -axnu / /mnt
rsync --delete -axHAWX --info=progress2 --numeric-ids / /mnt --exclude /proc


# 5- INIT RAM FS
echo "[*] Initramfs düzenleniyor.."
mountpoint -q /mnt/dev || mount --bind /dev /mnt/dev
mountpoint -q /mnt/sys || mount --bind /sys /mnt/sys
mountpoint -q /mnt/proc || mount --bind /proc /mnt/proc
chroot /mnt dracut -N --force --xz --omit systemd  /boot/initramfs


# 6- GRUB !!
echo "[*] Grub yükleniyor.."
grub-install --boot-directory=/mnt/boot /dev/$disk
if [ -d /mnt/var/lock ]; then rm -rf /mnt/var/lock; fi
chroot /mnt grub-mkconfig -o /boot/grub/grub.cfg

echo -e "[*] ISLEM TAMAMLANDI!!! \n[*] Cihazı yeniden başlatabilirsiniz."
