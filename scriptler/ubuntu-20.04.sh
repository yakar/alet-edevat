#!/bin/bash

# update & upgrade
sudo apt update && sudo apt -y upgrade


# Install OnlyOffice? neofetch? Ulauncher?
sudo apt -y install filezilla gimp grub-customizer guake keepassx meld virtualbox xclip
sudo apt -y install curl git gnome-tweaks nmap numix-icon-theme-circle tree ubuntu-restricted-extras vim zsh


# Vivaldi
if [ ! -f /usr/bin/vivaldi ]; then
    echo -e "\e[92m\n\n>>> Vivaldi\e[0m"
    wget 'https://downloads.vivaldi.com/stable/vivaldi-stable_3.0.1874.33-1_amd64.deb' -O /tmp/vivaldi.deb && sudo dpkg -i /tmp/vivaldi.deb
fi


# Code (Visual Studio Code)
if [ ! -f /usr/bin/code ]; then
    echo -e "\e[92m\n\n>>> Visual Studio Code\e[0m"
    wget 'https://go.microsoft.com/fwlink/?LinkID=760868' -O /tmp/code.deb && sudo dpkg -i /tmp/code.deb
    
    #install code extension
    # code --install-extension <xxxx>
fi


# Guake autostart
if [ ! -f ~/.config/autostart/guake.desktop ]; then
    echo -e "\e[92m\n\n>>> Guake (autostart)\e[0m"
    echo -e "[Desktop Entry]\nType=Application\nExec=/usr/bin/guake\nHidden=false\nNoDisplay=false\nX-GNOME-Autostart-enabled=true\nName=Guake" > ~/.config/autostart/guake.desktop
fi


# vimrc
echo -e "\e[92m\n\n>>> VimRC\e[0m"
sudo sed -ie 's/indianred/orangered/g' /usr/share/vim/vim81/colors/desert.vim #fix for E254 error :S
if [ ! -d ~/.vim_runtime ]; then
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_awesome_vimrc.sh
else
    ( cd ~/.vim_runtime; git pull --rebase )
fi


# Postman
if [ ! -d ~/Postman ]; then
    echo -e "\e[92m\n\n>>> Postman\e[0m"
    wget --no-check-certificate 'https://dl.pstmn.io/download/latest/linux64' -O /tmp/postman-x64.tar.gz
    tar -xf /tmp/postman-x64.tar.gz
    mv -f Postman ~/Postman
    echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Postman\nExec=$HOME/Postman/Postman\nIcon=$HOME/Postman/app/resources/app/assets/icon.png\nType=Application\nCategories=Network;" > ~/.local/share/applications/postman.desktop
fi


# burp suite free
if [ ! -d /home/aydin/BurpSuiteCommunity ]; then
    echo -e "\e[92m\n\n>>> Burp Suite Community\e[0m"
    wget 'https://portswigger.net/burp/releases/download?product=community&version=2020.4&type=Linux' -O /tmp/burpsuite.sh
    sh /tmp/burpsuite.sh
fi


# grub-customizer theme
if [ ! -d /boot/grub/themes/Aurora-Penguinis-GRUB2 ]; then
    echo -e "\e[92m\n\n>>> Grub Customizer\e[0m"
    wget --no-check-certificate 'https://gitlab.com/yakar/alet-edevat/raw/master/scriptler/elementary-loki-apps/Aurora-Penguinis-GRUB2.tar.gz' -O /tmp/Aurora-Penguinis-GRUB2.tar.gz
    tar -zxf /tmp/Aurora-Penguinis-GRUB2.tar.gz
    sudo mv -f Aurora-Penguinis-GRUB2 /boot/grub/themes/
fi


# Telegram
if [ ! -d ~/Telegram ]; then
    echo -e "\e[92m\n\n>>> Telegram\e[0m"
    wget --no-check-certificate 'https://telegram.org/dl/desktop/linux' -O /tmp/telegram.tar.gz
    cd /tmp && tar -xf /tmp/telegram.tar.gz
    mv -f Telegram ~/Telegram
    wget --no-check-certificate https://gitlab.com/yakar/alet-edevat/raw/master/scriptler/elementary-loki-apps/telegram.png -O ~/Telegram/icon.png
    echo -e "[Desktop Entry]\nEncoding=UTF-8\nName=Telegram\nExec=$HOME/Telegram/Telegram\nIcon=$HOME/Telegram/icon.png\nType=Application\nCategories=Network;" > ~/.local/share/applications/telegram.desktop
    cp ~/.local/share/applications/telegram.desktop ~/.config/autostart/
fi


# Dropbox
if [ ! -f /usr/bin/dropbox ]; then
    echo -e "\e[92m\n\n>>> Dropbox\e[0m"
    wget --no-check-certificate 'https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2020.03.04_amd64.deb' -O /tmp/dropbox.deb
    sudo dpkg -i /tmp/dropbox.deb
    sudo apt install python3-gpg
fi


# Spotify
if [ ! -f /usr/bin/spotify ]; then
    echo -e "\e[92m\n\n>>> Spotify\e[0m"
    curl -sS https://download.spotify.com/debian/pubkey.gpg | sudo apt-key add - 
    echo "deb http://repository.spotify.com stable non-free" | sudo tee /etc/apt/sources.list.d/spotify.list
    sudo apt update && sudo apt -y install spotify-client
fi


# Bashtop
if [ ! -f /usr/bin/bashtop ]; then
    echo -e "\e[92m\n\n>>> Bashtop\e[0m"
    sudo add-apt-repository ppa:bashtop-monitor/bashtop
    sudo apt update && sudo apt -y install bashtop
fi


# Oh My Zsh
if [ ! -d ~/.oh-my-zsh ]; then
    echo -e "\e[92m\n\n>>> Oh My Zsh\e[0m"
    sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
    sed -ie 's/ZSH_THEME="robbyrussell"/ZSH_THEME="lukerandall"/g' ~/.zshrc
fi


# Gnome settings
gsettings set org.gnome.shell.extensions.dash-to-dock dock-position BOTTOM
gsettings set org.gnome.shell.extensions.dash-to-dock dash-max-icon-size 24
gsettings set org.gnome.desktop.interface icon-theme "Numix-Circle" #https://askubuntu.com/questions/262868/how-to-set-icons-and-theme-from-terminal
gsettings set org.gnome.shell.extensions.dash-to-dock click-action 'minimize' # minimize on dash


# clean save files..
sudo rm /etc/apt/sources.list.d/*.save
