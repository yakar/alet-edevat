#!/bin/bash
#
# Aydin Yakar
# https://gitlab.com/yakar/alet-edevat/blob/master/scriptler/elementary-loki-apps-installer.sh
# 09 August 2018
#
# APPS:
# chromium-browser cryptomator dkms filezilla gimp git grub-customizer guake keepassx libreoffice libreoffice-gtk libreoffice-style-sifr mail-notification meld
# numix-icon-theme-circle opera-stable rdesktop simplescreenrecorder slack-desktop sqliteman vscodium uget wine1.6 winetricks vim vlc zsh
#
# binwalk hping3 ngrep nikto nmap rarcrack sqlmap tshark volatility wireshark burpsuite
#
# unace rar unrar p7zip-rar p7zip sharutils uudeview mpack arj cabextract lzip lunzip
#
# grub-customizer theme: Aurora Penguinis (Grub 2)
#
# Gimp 2 Photostop extention
#
# Telegram, Dropbox, f.lux
#
# Driver: Samsung SCX, Samsung M2070 Printers

set -e


# root check
if [ "$(id -u)" != "0" ]; then echo "Run with sudo: sudo $0"; exit 1; fi


# update & upgrade
apt update && apt -y upgrade && apt -y dist-upgrade


# extra repositories
apt -y install software-properties-common
add-apt-repository -y ppa:danielrichter2007/grub-customizer
add-apt-repository -y ppa:philip.scott/elementary-tweaks
apt-add-repository -y ppa:numix/ppa # numix iconset


# general apps
apt -y install chromium-browser dkms elementary-tweaks firefox filezilla gimp git grub-customizer guake keepassx libreoffice libreoffice-gtk libreoffice-style-sifr meld numix-icon-theme-circle rdesktop wine1.6 winetricks vim vlc zsh

# vim sublime / https://github.com/grigio/vim-sublime
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
curl https://raw.githubusercontent.com/grigio/vim-sublime/master/vimrc > $HOME/.vimrc
git clone https://github.com/flazz/vim-colorschemes
mv vim-colorschemes/colors/ ~/.vim
rm -rf vim-colorschemes/
# run vim and type :PluginInstall for installing plugins..

# Visual Studio Code
code --install-extension ms-vscode.go
code --install-extension vscode-icons-team.vscode-icons
code --install-extension angular.ng-template
code --install-extension aaron-bond.better-comments
code --install-extension felixfbecker.php-pack
code --install-extension bmewburn.vscode-intelephense-client
code --install-extension felixfbecker.php-intellisense
code --install-extension ikappas.phpcs
code --install-extension ms-python.python
code --install-extension formulahendry.vscode-mysql
code --install-extension ms-mssql.mssql
code --install-extension ms-ceintl.vscode-language-pack-tr
code --install-extension dotjoshjohnson.xml
code --install-extension ms-vscode.vscode-typescript-tslint-plugin
code --install-extension msjsdiag.debugger-for-chrome
code --install-extension johnpapa.angular2
code --install-extension bungcip.better-toml

# security/pentest/forensics apps
apt -y install binwalk hping3 ngrep nikto nmap rarcrack sqlmap tshark volatility wireshark

# burp suite free
if [ ! -f "/usr/share/applications/Burp Suite Free Edition-0.desktop" ]; then
    wget 'https://portswigger.net/burp/releases/download?product=community&version=2.1.04&type=linux' -O /tmp/burpsuite.sh
    sudo -u $SUDO_USER sh /tmp/burpsuite.sh
fi

# metasploit
#if [ ! -f /usr/bin/msfconsole ]; then
#    sudo -u $SUDO_USER curl https://raw.githubusercontent.com/rapid7/metasploit-omnibus/master/config/templates/metasploit-framework-wrappers/msfupdate.erb > msfinstall && \
#    chmod 755 msfinstall && \
#    ./msfinstall
#fi

# Archive Formats and Restricted Extras
apt -y install unace rar unrar p7zip-rar p7zip sharutils uudeview mpack arj cabextract lzip lunzip


# guake autostart
cp /usr/share/applications/guake.desktop /etc/xdg/autostart/


# grub-customizer theme
if [ ! -d /boot/grub/themes/Aurora-Penguinis-GRUB2 ]; then
    wget --no-check-certificate https://gitlab.com/yakar/alet-edevat/raw/master/scriptler/elementary-loki-apps/Aurora-Penguinis-GRUB2.tar.gz -O /tmp/Aurora-Penguinis-GRUB2.tar.gz
    tar -zxf /tmp/Aurora-Penguinis-GRUB2.tar.gz
    mv -f Aurora-Penguinis-GRUB2 /boot/grub/themes/
fi

# Opera Stable
if [ ! -f /usr/bin/opera ]; then
    wget --no-check-certificate 'https://www.opera.com/download/get/?id=45295&amp;location=415&amp;nothanks=yes&amp;sub=marine&utm_tryagain=yes' -O /tmp/opera-stable.deb
    dpkg -i /tmp/opera-stable.deb
fi


# gimp to photoshop :)
sudo -u $SUDO_USER sh -c "$(wget https://raw.githubusercontent.com/doctormo/GimpPs/master/tools/install.sh -O -)"


# Telegram
if [ ! -d ~/Telegram ]; then
    sudo -u $SUDO_USER wget --no-check-certificate https://telegram.org/dl/desktop/linux -O /tmp/telegram.tar.gz
    cd /tmp && tar -xf /tmp/telegram.tar.gz
    mv -f Telegram ~/Telegram
    sudo -u $SUDO_USER wget --no-check-certificate https://gitlab.com/yakar/alet-edevat/raw/master/scriptler/elementary-loki-apps/telegram.png -O ~/Telegram/icon.png
    echo "
[Desktop Entry]
Encoding=UTF-8
Name=Telegram
Exec=$HOME/Telegram/Telegram
Icon=$HOME/Telegram/icon.png
Type=Application
Categories=Network;
    " | tee $HOME/.local/share/applications/telegram.desktop
    cp $HOME/.local/share/applications/telegram.desktop /etc/xdg/autostart/
fi



# Dropbox
if [ ! -f /usr/bin/dropbox ]; then
    wget --no-check-certificate https://www.dropbox.com/download?dl=packages/ubuntu/dropbox_2019.02.14_amd64.deb -O /tmp/dropbox.deb
    dpkg -i /tmp/dropbox.deb
    apt -f install
fi



# Samsung SCX, Samsung M2070 driver
wget --no-check-certificate https://gitlab.com/yakar/alet-edevat/raw/master/scriptler/elementary-loki-apps/ULD_v1.00.29.tar.gz -O /tmp/ULD_v1.00.29.tar.gz
(
cd /tmp
tar -zxf ULD_v1.00.29.tar.gz
sh uld/install.sh
)



# Postman
if [ ! -d ~/Postman ]; then
    sudo -u $SUDO_USER wget --no-check-certificate https://dl.pstmn.io/download/latest/linux64 -O /tmp/postman-x64.tar.gz
    tar -xf /tmp/postman-x64.tar.gz
    mv -f Postman ~/Postman
    echo "
[Desktop Entry]
Encoding=UTF-8
Name=Postman
Exec=$HOME/Postman/Postman
Icon=$HOME/Postman/app/resources/app/assets/icon.png
Type=Application
Categories=Network;
    " | tee $HOME/.local/share/applications/postman.desktop
fi



# awesome vimrc
if [ ! -d ~/.vim_runtime ]; then
    git clone --depth=1 https://github.com/amix/vimrc.git ~/.vim_runtime
    sh ~/.vim_runtime/install_awesome_vimrc.sh
else
    ( cd ~/.vim_runtime; git pull --rebase )
fi

# zsh & oh-my-zsh
sh -c "$(curl -fsSL https://raw.github.com/robbyrussell/oh-my-zsh/master/tools/install.sh)"
sed -ie 's/ZSH_THEME="robbyrussell"/ZSH_THEME="lukerandall"/g' ~/.zshrc